package com.project.in;

public class Guru {

  private int id;
  private String name;
  private int age;
  private int standarGaji;
  private static int perolehanGaji;

  public Guru(int id, String name, int age, int standarGaji){
    this.id = id;
    this.name = name;
    this.age = age;
    this.standarGaji = standarGaji;
    perolehanGaji = 0;
  }

  public void mendapatkanGaji(int gaji) {
    perolehanGaji += gaji;
    Sekolah.updateTotalPengeluaran(gaji);
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public int getAge() {
    return age;
  }

  public int getStandarGaji() {
    return standarGaji;
  }

  public static int getPerolehanGaji() {
    return perolehanGaji;
  }
}
