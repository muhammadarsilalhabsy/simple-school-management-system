package com.project.in;

import java.util.List;

public class Sekolah {
  private String name;
  private List<Siswa> siswa;
  private List<Guru> guru;
  private static int totalKhas;

  private int pengeluaran;


  public Sekolah(String name, List<Siswa> siswa, List<Guru> guru){
    this.name = name;
    this.siswa = siswa;
    this.guru = guru;
    totalKhas = 0;
  }
  public static void updateTotalKhas(int pembayaran){
    totalKhas += pembayaran;
  }

  public static void updateTotalPengeluaran(int membayar){
    totalKhas -= membayar;
  }

  public void addGuru(Guru guru){
    this.guru.add(guru);
  }

  public void addSiswa(Siswa siswa){
    this.siswa.add(siswa);
  }

  public String getName() {
    return name;
  }

  public List<Siswa> getSiswa() {
    return siswa;
  }

  public List<Guru> getGuru() {
    return guru;
  }

  public static int getTotalKhas() {
    return totalKhas;
  }

  public int getTotalPengeluaran() {
    return this.pengeluaran += Guru.getPerolehanGaji();
  }
}


