package com.project.in;

import java.util.ArrayList;
import java.util.List;

public class Sma {
  public static void main(String[] args) {

    Guru laRumpu = new Guru(1, "La Rumpu", 54, 2_000_000);
    Guru sarifudin = new Guru(2, "Sarifudin", 48, 2_050_000);
    Guru mima = new Guru(3, "Mima", 51, 2_050_000);
    Guru taty = new Guru(4, "Taty", 45, 3_000_000);


    List<Guru> guruList = new ArrayList<>();
    guruList.add(laRumpu);
    guruList.add(sarifudin);
    guruList.add(mima);
    guruList.add(taty);

    Siswa arsil = new Siswa(1,"Muhammad Arsil Alhabsy", 17);
    Siswa nova = new Siswa(2,"Nova Aryani Saputri Badri", 17);
    Siswa aisa = new Siswa(3,"Aisa ratna sedonya", 17);
    Siswa ludiah = new Siswa(4,"Ludia wati", 17);


    List<Siswa> siswaList = new ArrayList<>();
    siswaList.add(arsil);
    siswaList.add(nova);
    siswaList.add(aisa);
    siswaList.add(ludiah);

    Sekolah smanti = new Sekolah("SMK 3 Kendari",siswaList, guruList);

    // siswa membayar
    arsil.updatePembayaran(100_000);
    nova.updatePembayaran(205_000);
    ludiah.updatePembayaran(205_000);

    // melihat sisa pembayaran
    System.out.println("Sisa pembayaran siswa yang bernama " + arsil.getName() + " $" +arsil.getSisaPembayaran());
    System.out.println("Sisa pembayaran siswa yang bernama " + nova.getName() + " $" +nova.getSisaPembayaran());
    System.out.println("Sisa pembayaran siswa yang bernama " + ludiah.getName() + " $" +ludiah.getSisaPembayaran());

    // melihat uang yang didapatkan oleh sekolah
    System.out.println("Sekolah menerima uang sebanyak $" + Sekolah.getTotalKhas());

    // sekolah membayar gaji guru
    laRumpu.mendapatkanGaji(105_000);
    sarifudin.mendapatkanGaji(235_000);

    // total pengeluaran sekolah membayar gaji guru
    System.out.println("Sekolah mengeluarkan uang sebanyak $" + smanti.getTotalPengeluaran());
    System.out.println("Sisa saldo sekolah adalah sebanyak $" + Sekolah.getTotalKhas());

    System.out.println("\n====Siswa====");
    for (Siswa siswa: siswaList) {
      System.out.println(siswa.getName());
    }

    System.out.println("\n====Guru====");
    for (Guru guru: guruList) {
      System.out.println(guru.getName());
    }

  }
}
