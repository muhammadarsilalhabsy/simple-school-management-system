package com.project.in;

public class Siswa {
  private int id;
  private String name;
  private int age;
  private int pembayaran;
  private int totalPembayaran;

  public Siswa(int id, String name, int age){
    this.id = id;
    this.name = name;
    this.age = age;
    this.pembayaran = 0;
    this.totalPembayaran = 2_000_000;
  }

  public int getSisaPembayaran(){
    return totalPembayaran - pembayaran;

  }

  public void updatePembayaran(int bayar){
    this.pembayaran += bayar;
    Sekolah.updateTotalKhas(bayar);
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public int getAge() {
    return age;
  }
}
