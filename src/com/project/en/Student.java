package com.project.en;

public class Student {
  private final int id;
  private final String name;
  private int grade;
  private int feesPaid;
  private final int feesTotal;

  public Student(int id, String name, int grade) {
    this.id = id;
    this.name = name;
    this.grade = grade;
    this.feesPaid = 0;
    this.feesTotal = 3000;
  }

  public void setGrade(int grade) {
    this.grade = grade;
  }

  public void payFees(int fees) {
    this.feesPaid += fees;
    School.updateTotalMoneyEarned(feesPaid);
  }

  public int getRemainingFess() {
    return this.feesTotal - this.feesPaid;
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public int getGrade() {
    return grade;
  }

  public int getFeesPaid() {
    return feesPaid;
  }

  public int getFeesTotal() {
    return feesTotal;
  }

}
