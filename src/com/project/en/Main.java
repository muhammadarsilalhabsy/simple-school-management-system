package com.project.en;

import java.util.ArrayList;
import java.util.List;

public class Main {
  public static void main(String[] args) {
    System.out.println("SMK Negeri 3000 Kendari");

    Teacher otong = new Teacher(1, "Pak Otong", 2000);
    Teacher jamal = new Teacher(2, "Pak jamal", 500);
    Teacher tuti = new Teacher(3, "Bu Tuti", 900);

    List<Teacher> teacherList = new ArrayList<>();
    teacherList.add(otong);
    teacherList.add(jamal);
    teacherList.add(tuti);

    Student ronald = new Student(1, "Ronald", 4);
    Student sarif = new Student(1, "Sarif", 14);
    Student yuyun = new Student(3, "Yuyun", 24);

    List<Student> studentList = new ArrayList<>();
    studentList.add(ronald);
    studentList.add(sarif);
    studentList.add(yuyun);

    School school = new School(teacherList, studentList);

    ronald.payFees(2000);
    System.out.println("School has earned $" + school.getTotalMoneyEarned());
    sarif.payFees(3000);
    System.out.println("School has earned $" + school.getTotalMoneyEarned());

    System.out.println("=======School pay all teacher=======");

    otong.receivedSalary(otong.getSalary());

    System.out.println("School must pay for " + otong.getName() + " and now School has $" + school.getTotalMoneyEarned());

    jamal.receivedSalary(jamal.getSalary());

    System.out.println("School must pay for " + jamal.getName() + " and now School has $" + school.getTotalMoneyEarned());


  }
}
